<?php

return array(
	'version' => '0.9.4.1',
	'browsercache.configuration_sealed' => false,
	'cdn.configuration_sealed' => false,
	'common.install' => 1456028382,
	'common.visible_by_master_only' => true,
	'dbcache.configuration_sealed' => true,
	'minify.configuration_sealed' => true,
	'objectcache.configuration_sealed' => true,
	'pgcache.configuration_sealed' => true,
	'previewmode.enabled' => false,
	'varnish.configuration_sealed' => false,
	'fragmentcache.configuration_sealed' => false,
	'newrelic.configuration_sealed' => false,
	'extensions.configuration_sealed' => array(
	),
	'notes.minify_error' => false,
	'minify.error.last' => '',
	'minify.error.notification' => 'email',
	'minify.error.notification.last' => 0,
	'minify.error.file' => '',
	'track.maxcdn_signup' => 0,
	'track.maxcdn_authorize' => 0,
	'track.maxcdn_validation' => 0,
	'notes.maxcdn_whitelist_ip' => true,
	'notes.remove_w3tc' => false,
	'notes.hide_extensions' => array(
	),
	'evaluation.reminder' => 1466432634,
);